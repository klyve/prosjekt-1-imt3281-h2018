package no.ntnu.imt3281.sudoku;

import static org.junit.Assert.*;

import org.junit.Test;

public class SudokuTest {

	@Test
	public void testEmptyConstructor() {
		Sudoku sudoku = new Sudoku();
		assertTrue(sudoku instanceof Sudoku);
	}

	@Test
	public void testSwaps() {
		Sudoku sudoku = new Sudoku();
		Integer[][] matrix = {
				{5, -1, -1, -1, 7, -1, -1, -1, -1},
				{-1, -1, -1, 1, -1, 5, -1, -1, -1},
				{-1, 9, 8, -1, -1, -1, -1, 6, -1},
				{8, -1, -1, -1, 6, -1, -1, -1, -1},
				{-1, -1, -1, 8, -1, 3, -1, -1, 1},
				{-1, -1, -1, -1, 2, -1, -1, -1, 6},
				{-1, 6, -1, -1, -1, -1, 2, -1, -1},
				{-1, -1, -1, 4, 1, 9, -1, -1, 5},
				{-1, -1, -1, -1, -1, -1, -1, -1, 9}
		};
		Integer[][] board = JSONBoard.parseFile("hard");
		Integer[][] currentBoard = Matrix.copy(board);
		matrix = Matrix.colSwap(matrix);
		board = Matrix.colSwap(board);
		for(int i = 0; i < 9; ++i) {
			assertArrayEquals(matrix[i], currentBoard[i]);
		}
	}
}

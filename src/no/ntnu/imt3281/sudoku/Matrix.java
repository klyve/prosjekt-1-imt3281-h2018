package no.ntnu.imt3281.sudoku;
import java.util.Random;

public class Matrix {

    /**
     * colSwap swaps the columns
     * @param current current board layout
     * @return new board layout Integer[][]
     */
    public static Integer[][] colSwap(Integer[][] current) {
        int tmp;
        for (int i = 0; i < current.length; i++) {
            for (int j = 0; j < current[i].length / 2; j++) {
                tmp = current[i][j];
                current[i][j] = current[i][current.length - 1 - j];
                current[i][current.length - 1 -j] = tmp;
            }
        }
        return current;
    }
    /**
     * rowSwap swaps the rows
     * @param current current board layout
     * @return new board layout Integer[][]
     */
    public static Integer[][] rowSwap(Integer[][] current) {
        int tmp;
        for (int i = 0; i < current.length / 2; i++) {
            for (int j = 0; j < current[i].length; j++) {
                tmp = current[i][j];
                current[i][j] = current[current.length - 1 - i][j];
                current[current.length - 1 -i][j] = tmp;
            }
        }
        return current;
    }

    /**
     * swapDiagonalRight swaps on the right diagonal
     * @param current current board layout
     * @return new board layout Integer[][]
     */
    public static Integer[][] swapDiagonalRight(Integer[][] current) {
        Integer[][] newList = new Integer[9][9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                newList[i][j] = current[j][i];
            }
        }

        return newList;
    }

    /**
     * swapDiagonalLeft Swaps diagonal left
     * @param current current board layout
     * @return new board layout Integer[][]
     */
    public static Integer[][] swapDiagonalLeft(Integer[][] current) {
        Integer[][] newList = new Integer[9][9];


        for (int i = 0; i < 9; i++) {
            for (int j = 8; j >= 0; j--) {
                newList[i][j] = current[j][i];
            }
        }
        return newList;
    }

    /**
     * randomSwap swaps random numbers around
     * @param current current board layout
     * @param times the amount of times to run the swaps
     * @return new board layout Integer[][]
     */
    public static Integer[][] randomSwap(Integer[][] current, int times) {
        Random rand = new Random();

        for(int i = 0; i < times; ++i) {
           // int  n1 = rand.nextInt(9) + 1;
            int n1 = (int)(Math.random() * 9 + 1);
            int n2 = (int)(Math.random() * 9 + 1);
            if(n1 == n2) continue;

            for(int j = 0; j < 9; j++) {
                for(int x = 0; x < 9; x++) {
                    if(current[j][x] == n1) {
                        current[j][x] = n2;
                    }else if(current[j][x] == n2) {
                        current[j][x] = n1;
                    }
                }
            }

        }
        return current;
    }

    /**
     * swaps Randomize the swap methods
     * @param current current board layout
     * @param times the amount of times to run the swaps
     * @return new board layout Integer[][]
     */
    public static Integer[][] swaps(Integer[][] current, int times) {
        Random rand = new Random();

        for(int i = 0; i < times; ++i) {
            int  n1 = rand.nextInt(5) + 1;
            switch(n1) {
                case 1: {
                    current = randomSwap(current, 4);
                }break;
                case 2: {
                    current = rowSwap(current);
                }break;
                case 3: {
                    current = colSwap(current);
                }break;
                case 4: {
                    current = swapDiagonalRight(current);
                }break;
                case 5: {
                    current = swapDiagonalLeft(current);
                }break;
            }


        }

        return current;
    }

    /**
     * copy Creates a copy of a given array
     * @param current current board layout
     * @return new board layout copy Integer[][]
     */
    public static Integer[][] copy(Integer[][] current) {
        Integer[][] newList = new Integer[9][9];


        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                newList[i][j] = current[j][i];
            }
        }
        return newList;
    }
}

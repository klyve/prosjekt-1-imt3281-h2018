package no.ntnu.imt3281.sudoku;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.io.FileNotFoundException;
import java.io.IOException;


public class JSONBoard {

    /**
     * parseFile parses a json sudoku board
     * @param name name of the file to use
     * @return board layout Integer[][]
     */
    public static Integer[][] parseFile(String name) {
        JSONParser parser = new JSONParser();
        Integer[][] board = new Integer[9][9];
        try {
            JSONArray a = (JSONArray) parser.parse(new FileReader("src/assets/games/"+name+".json"));
            Iterator i = a.iterator();
            int rowCount = 0;
            // Loop over rows and add them to the board
            while(i.hasNext()) {
                JSONArray rows= (JSONArray) i.next();

                Iterator row = rows.iterator();
                int cellCount = 0;
                while(row.hasNext()) {
                    Integer obj = ((Long) row.next()).intValue();
                    board[rowCount][cellCount] = obj;
                    cellCount++;
                }
                rowCount++;

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return board;
    }


}

package no.ntnu.imt3281.sudoku;

public class BadNumberException extends Exception {

    private static final long serialVersionUID = 7718828512143293558L;

    public BadNumberException() {
        super();
    }

    public BadNumberException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public BadNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public BadNumberException(String message) {
        super(message);
    }
    public BadNumberException(int row, int col, int value) {
        super(String.format("Number on row %d column %d with value %d already exists in row, column or 3x3 grid", row, col, value));
    }

    public BadNumberException(Throwable cause) {
        super(cause);
    }
}
package no.ntnu.imt3281.sudoku;
import javafx.application.Application;
import javafx.css.PseudoClass;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.Cursor;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Alert;
import javafx.scene.Node;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Iterator;

public class Sudoku extends Application {

    protected Integer[][] currentBoard;
    protected Integer[][] originalBoard;
    protected GridPane grid;
    protected GridPane board;

    /**
     * This is the main method that starts up the sudoku board
     * @param Stage primaryStage Primary stage used to display the javafx instance
     * @return Nothing.
     */
    @Override
    public void start(Stage primaryStage) {
        try {
            //Solve Button
            Button solveButton = new Button("Solve");
            solveButton.setMaxWidth(Double.MAX_VALUE);

            //Reset Button
            Button resetButton = new Button("Reset");
            resetButton.setMaxWidth(Double.MAX_VALUE);

            Button hardPuzzle = new Button("New Hard puzzle");
            hardPuzzle.setMaxWidth(Double.MAX_VALUE);


            Button easyPuzzle = new Button("New Easy puzzle");
            easyPuzzle.setMaxWidth(Double.MAX_VALUE);

            //Grid
            grid = new GridPane();
            grid.setPadding(new Insets(40, 40, 40, 40));

            //Setting the grid to the scene.
            Scene scene = new Scene(grid);

            // Will hold the 2 buttons in a vBox.
            VBox vb = new VBox();
            vb.setPadding(new Insets(10, 30, 0, 30));
            vb.setSpacing(10);
            vb.getChildren().addAll(solveButton, resetButton, hardPuzzle, easyPuzzle);

            vb.setStyle("-fx-background-color: #333333");

            GridPane.setRowIndex(vb, 0);
            GridPane.setColumnIndex(vb, 4);

            // Adds in the vBox consisting of the 2 buttons onto the GridPane.
            grid.getChildren().add(vb);





            board = new GridPane();

            PseudoClass right = PseudoClass.getPseudoClass("right");
            PseudoClass bottom = PseudoClass.getPseudoClass("bottom");
            currentBoard = JSONBoard.parseFile("simple");
            currentBoard = Matrix.swaps(currentBoard, 100);
            originalBoard = Matrix.copy(currentBoard);


            for (int col = 0; col < 9; col++) {
                for (int row = 0; row < 9; row++) {
                    StackPane cell = new StackPane();
                    cell.getStyleClass().add("cell");

                    cell.pseudoClassStateChanged(right, col == 2 || col == 5);
                    cell.pseudoClassStateChanged(bottom, row == 2 || row == 5);
                    TextField field = createTextField(col, row);
                    if (currentBoard[col][row] != -1) {
                        field.getStyleClass().add("locked");
                        field.setDisable(true);
                        field.setText(Integer.toString(currentBoard[col][row]));
                    }
                    field.setId(String.format("%d-%d", col, row));
                    cell.getChildren().add(field);

                    board.add(cell, col, row);
                }
            }
            grid.getChildren().add(board);

            //Action Listeners for the buttons.
            try {
                solveButton.setOnAction(e -> {
                    if(checkForSuccessGeneral()) {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle(" Congratulations ");
                        alert.setHeaderText(null);
                        alert.setContentText(" You solved the board ");
                        alert.showAndWait();
                        resetBoard();
                    }else {
                        Alert alert = new Alert(AlertType.INFORMATION);
                        alert.setTitle(" Invalid Sudoku board! ");
                        alert.setHeaderText(null);
                        alert.setContentText(" Your input is invalid! \n"
                                + " Please check your board and try again.");
                        alert.showAndWait();
                    }

                });

                resetButton.setOnAction(e -> {
                    resetBoard();
                });

                hardPuzzle.setOnAction(e -> {
                    startPuzzle("hard");
                });

                easyPuzzle.setOnAction(e -> {
                    startPuzzle("simple");
                });

                //Shows hand when hovered over.
                solveButton.setOnMouseEntered(e -> {
                    solveButton.setCursor(Cursor.HAND);
                });

                resetButton.setOnMouseEntered(e -> {
                    resetButton.setCursor(Cursor.HAND);
                });
                hardPuzzle.setOnMouseEntered(e -> {
                    solveButton.setCursor(Cursor.HAND);
                });
                easyPuzzle.setOnMouseEntered(e -> {
                    solveButton.setCursor(Cursor.HAND);
                });

            } catch (Exception e) { // In case there is no solution. Not likely.
                Alert alert = new Alert(AlertType.INFORMATION);
                alert.setTitle(" No Solution Found! ");
                alert.setHeaderText(null);
                alert.setContentText(
                        "Please check your board and try again. ");
                alert.showAndWait();
            }


            scene.getStylesheets().add("/board.css");
            primaryStage.setScene(scene);
            primaryStage.show();
        }catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This function fills the board with contehnt from the sudoku matrix
     * @return Nothing.
     */
    protected void fillBoard() {
        Iterator<Node> iter = board.getChildren().iterator();
        while(iter.hasNext()) {
            Node item = iter.next();
            if(item instanceof StackPane) {
                StackPane node = ((StackPane)item);
                Iterator<Node> nIter = node.getChildren().iterator();
                while(nIter.hasNext()) {
                    Node nextItem = nIter.next();
                    if(nextItem instanceof TextField) {
                        TextField txt = (TextField) nextItem;
                        String id = txt.getId();
                        String[] parts = id.split("-");
                        int col = Integer.parseInt(parts[0]);
                        int row = Integer.parseInt(parts[1]);
                        int num = currentBoard[col][row];
                        if(num == -1) continue;
                        txt.getStyleClass().add("locked");
                        txt.setDisable(true);
                        txt.setText(Integer.toString(currentBoard[col][row]));
                    }
                }
            }
        }
    }
    /**
     * This function resets the board, removes all entries on the board and clear all fields
     * And calls the fillboard function
     * @return Nothing.
     */
    protected void resetBoard() {
        currentBoard = Matrix.copy(originalBoard);
        Iterator<Node> iter = board.getChildren().iterator();
        while(iter.hasNext()) {
            Node item = iter.next();
            if(item instanceof StackPane) {
                StackPane node = ((StackPane)item);
                Iterator<Node> nIter = node.getChildren().iterator();
                while(nIter.hasNext()) {
                    Node nextItem = nIter.next();
                    if(nextItem instanceof TextField) {
                        TextField txt = (TextField) nextItem;
                        txt.setText("");
                        txt.setDisable(false);
                        txt.getStyleClass().remove("locked");
                        txt.getStyleClass().remove("invalid");
                    }
                }
            }
        }
        fillBoard();
    }

    /**
     * Creates a text field for the sudoku board, with text event listener
     * @param col int Contains the column number where this is being inserted
     * @param row int Contains the row number where this is being inserted
     * @return TextField
     */
    private TextField createTextField(int col, int row) {
        TextField textField = new TextField();

        // restrict input to integers
        // Make sure they are valid inputs
        textField.setTextFormatter(new TextFormatter<Integer>(c -> {
            if (c.getControlNewText().matches("^[1-9]?")) {
                String text = c.getText();
                if (text != "") {
                    currentBoard[col][row] = Integer.parseInt(text);
                    try {
                        checkValidInput(row, col, Integer.parseInt(text));
                        textField.getStyleClass().remove("invalid");
                    }catch(BadNumberException err) {
                        textField.getStyleClass().add("invalid");
                    }
                }

                return c;
            } else {
                return null;
            }
        }));
        return textField;
    }
    /**
     * main method
     * @param args used in launch
     * @return Nothing.
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Starts a new puzzle
     * @param type type of puzzle to start
     * @return Nothing.
     */
    public void startPuzzle(String type) {
        currentBoard = JSONBoard.parseFile(type);
        currentBoard = Matrix.swaps(currentBoard, 100);
        originalBoard = Matrix.copy(currentBoard);
        resetBoard();
    }

    /**
     * Get row returns a iterable row object by number 0-8
     * @param row number of row to get
     * @return iterable integer
     */
    public Iterable<Integer> getRow(int row) {
        Integer[] currentRow = new Integer[9];
        for(int i = 0; i < 9; ++i) {
            currentRow[i] = currentBoard[i][row];
        }
        return Arrays.asList(currentRow);
    }

    /**
     * getColumn returns an iterable column object by number 0-8
     * @param column the column to get
     * @return Iterable integer
     */
    public Iterable<Integer> getColumn(int column) {
        Integer[] currentColumn = currentBoard[column];
        return Arrays.asList(currentColumn);
    }

    /**
     * getGrid returns a 3x3 square decided by a number from 0-8 from left to right top to bottom
     * @param number the grid number to fetch
     * @return Iterable integer
     */
    public Iterable<Integer> getGrid(int number) {
        int col = 0;
        int row = 0;
        switch (number) {
            case 1: {
                col = 3;
            } break;
            case 2: {
                col = 6;
            } break;
            case 3: {
                row = 3;
            } break;
            case 4: {
                row = 3;
                col = 3;
            } break;
            case 5: {
                row = 3;
                col = 6;
            } break;
            case 6: {
                row = 6;
            } break;
            case 7: {
                row = 6;
                col = 3;
            } break;
            case 8: {
                row = 6;
                col = 6;
            } break;
        }


        List<Integer> currentGrid = new ArrayList<Integer>();
        for(int i = col; i < col+3; ++i) {
            for(int j = row; j < row+3; ++j) {
                currentGrid.add(currentBoard[i][j]);
            }
        }

        return currentGrid;

    }

    /**
     * squareNumber gets the grid number by row and column
     * @param row which row to target
     * @param col which column to target
     * @return int
     */
    public int squareNumber(int row, int col) {
        int majorRow = (row) / 3;
        int majorCol = (col) / 3;
        return majorCol + majorRow * 3;
    }

    /**
     * checkValidInput checks if the input is valid according to sudoku rules by row col and value
     * @param row the current input row
     * @param col the current input col
     * @param value the current inputs value
     * @throws BadNumberException
     * @return Nothing.
     */
    public void checkValidInput(int row, int col, int value) throws BadNumberException {
        if(value == -1) return;

        // Dirty way to solve the issue of the object finding itself...
        int matches = 0;


        Iterable<Integer> rows = getRow(row);
        Iterator<Integer> rowIter = rows.iterator();

        while(rowIter.hasNext()) {
            if(rowIter.next() == value)
                matches++;
        }
        if(matches > 1) throw new BadNumberException(row, col, value);


        Iterable<Integer> columns = getColumn(col);
        Iterator<Integer> colIter = columns.iterator();

        matches = 0;
        while(colIter.hasNext()) {
            if(colIter.next() == value) matches++;
        }
        if(matches > 1) throw new BadNumberException(row, col, value);


        int gridNum = squareNumber(row, col);

        Iterable<Integer> grid = getGrid(gridNum);
        Iterator<Integer> gridIter = grid.iterator();
        matches = 0;
        while(gridIter.hasNext()) {
            if(gridIter.next() == value) matches++;
        }
        if(matches > 1) throw new BadNumberException(row, col, value);

        return;
    }


    /**
     * checkForSuccessGeneral checks if the player has won accorxing to the sudoku rules
     * @return true if the playter has won!
     */
    public boolean checkForSuccessGeneral() {
        // Dirty way to solve the issue of the object finding itself...
        int sum = 0;
        for(int i = 0; i < 9; ++i) {
            Iterable<Integer> rows = getRow(i);
            Iterator<Integer> rowIter = rows.iterator();
            while(rowIter.hasNext()) {
                sum += rowIter.next();
            }
            if(sum != 45) return false;


            Iterable<Integer> columns = getColumn(i);
            Iterator<Integer> colIter = columns.iterator();
            while(colIter.hasNext()) {
                sum += colIter.next();
            }
            if(sum != 45) return false;


            Iterable<Integer> grid = getGrid(i);
            Iterator<Integer> gridIter = grid.iterator();
            while(gridIter.hasNext()) {
                sum += gridIter.next();
            }
            if(sum != 45) return false;
        }

        return true;
    }


}
